
Simple routines for taking estimated occurrence values for all space, and plotting weekly bands of areas that had their year's first occurrence.  These create the "chromatography plots", showing the advancement of the migration front.
